# Equipo 05

## Nombre del proyecto
Planazo, un sitio de reservas de experiencias turísticas inolvidables.
Proyecto final de la carrera Certified Tech Developer - Digital House

## Integrantes
- Ibarra Ibañez, William
- Kassardjian, Pedro
- Ocampo, Maximiliano
- Ortiz Clavijo, Grisel,
- Sallietti, María Luz
- Segurado Nicolás

## Descripción
Este proyecto consiste en una aplicación capaz de realizar las operaciones básicas de creación, lectura, actualización y eliminación (CRUD) de reservas, productos, usuarios, categorías, y ciudades. La aplicación está compuesta por una API RESTful implementada con Spring Boot y una interfaz de usuario (UI) implementada con la biblioteca de JavaScript React y Typescript.

La API RESTful de Spring Boot proporciona los endpoints necesarios para interactuar con los datos y la interfaz de usuario implementada con React ofrece una experiencia intuitiva y amigable para que los usuarios puedan interactuar con la aplicación de manera fácil y eficiente.

## Tecnologías utilizadas

- Java
- Spring Boot
- React.js
- Typescript

## Ver online
[Ir a Planazo](http://planazo.s3-website.us-east-2.amazonaws.com/)

## Documentación

 [Accede a la documentación de los enpoints en Swagger](http://3.15.250.255:8080/swagger-ui/index.html )